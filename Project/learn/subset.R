setwd("~/MSC-LAB/datascience-lab/Project")

converter = function(r){
  r['Price'] = strtoi(r['Price'])
  r['LandSize'] = strtoi(r['LandSize'])
}

prune = function(){
  H = read.csv("data/Melbourne.csv")
  R = H[!is.na(H$Price) & !is.na(H$Landsize) & (H$Landsize != 0) & (H$Price != 'NA'),]
  R = H[(H$Method == 'S') | (H$Method == 'SP') | (H$Method == 'PN') | (H$Method == 'SN') | (H$Method == 'SA') | (H$Method == 'SS'),]
  apply(R,1,converter)
  write.csv(R,"data/Melbourne_pruned.csv")
}


prune()