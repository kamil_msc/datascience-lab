setwd("~/MSC-LAB/datascience-lab/Project")
library(ggplot2)


expensive_area = function(R,file_name,title){
  agg_data = aggregate(R,list(R$Region),mean)
  agg_data = data.frame(agg_data$Group.1,agg_data$PPU)
  names(agg_data) = c("Region","Avg.Price")
  agg_data = agg_data[order(-agg_data$Avg.Price),]
  top_3 = head(agg_data,3)
  png(file = paste("images/",file_name, sep=""))
  
  barplot(top_3$Avg.Price,
          main = paste("Expensive ",title),
          xlab = "Price",
          ylab = title,
          names.arg = top_3$Region,
          col = "darkred",
          horiz = TRUE)
  dev.off()
  print("Plotted")
  return(head(agg_data,1)$Region)
}

expensive_region = function(D){
  H = data.frame(D$PPU,D$Regionname)
  names(H) = c("PPU","Region")
  return(expensive_area(H,"expensive_region.png","Region"))
}

expensive_suburban = function(D){
  H = data.frame(D$PPU,D$Suburb)
  names(H) = c("PPU","Region")
  return(expensive_area(H,"expensive_suburban.png","Suburban"))
}

unit_price = function(r){
  p = strtoi(r['Price']) / strtoi(r['Landsize'])
  return(p)
}

D = read.csv("data/Melbourne_pruned.csv")
er = expensive_region(D)
print(paste("Expensive Region is ",er))




D = D[D$Regionname==er,]
es = expensive_suburban(D)
print(paste("Expensive suburban is ",es, " under ",er))
